package System

import (
	"errors"
	"fmt"
)

func (n *Node) StartProcess(name string, options ProcessOptions, executor ExecutorInterface, args []string) (*Process, error) {
	return NewProcess(name, options, executor, args, nil)
}

//
func (n *Node) StartProcessByExecutorName(processName string, options ProcessOptions, executorName string, args []string) (*Process, error) {
	var err error
	executor := n.GetExecutor(executorName)
	if executor == nil {
		err = errors.New(fmt.Sprintf("node %s does not have the executor: %s", n.ID, executorName))
		return nil, err
	}
	return n.StartProcess(processName, options, executor, args)
}

func (n *Node) KillProcess(pid string, reason string) error {
	process, exists := n.processes[pid]
	if exists == false {
		return errors.New("process not found")
	}
	return process.Kill(reason)
}
