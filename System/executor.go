package System

type ExecutorType string

type ExecutorInterface interface {
	Init(process *Process, args []string) error
	DeInit(reason string) error
}
