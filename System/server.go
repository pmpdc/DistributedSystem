package System

import (
	"errors"
	"fmt"
	"github.com/golang/protobuf/proto"
	systemProto "gitlab.com/pmpdc/DistributedSystem/proto"
	"gitlab.com/pmpdc/Network"
	"net"
)

const (
	SystemSyncPath   = "/system/sync"
	ProcessStartPath = "/process/start"
)

func (s *System) startLocalServer(port int) error {
	router := Network.NewRouter()

	router.Add(SystemSyncPath, func(ctx *Network.Context) {
		println("Server got message to /system/sync")
		var err error
		remoteSystem := &systemProto.SyncSystemMessage{}

		err = proto.Unmarshal(ctx.Message.Payload(), remoteSystem)
		if err != nil {
			goto Response
		}

		// can never be nil
		if remoteSystem.System.Nodes == nil || len(remoteSystem.System.Nodes) == 0 {
			err = errors.New("remote payload nodes cannot be nil or empty")
			goto Response
		}
		err = s.Merge(remoteSystem.System)
	Response:
		var errorString string
		if err != nil {
			errorString = err.Error()
		}
		responseBytes, err := proto.Marshal(&systemProto.SyncSystemMessage{
			System:  &s.System,
			Success: err == nil,
			Error:   errorString,
		})
		err = ctx.Write(responseBytes)
		if err != nil {
			println(fmt.Sprintf("\t Error: %s", err.Error()))
		} else if errorString != "" {
			println("\t" + errorString)
		} else {
			println("\tSuccess")
		}
	})

	router.Add(ProcessStartPath, func(ctx *Network.Context) {
		println("Server got message to /process/start")
		defer ctx.Conn.Close()
		var err error
		var process *Process
		var startProcessMessage systemProto.StartProcessMessage

		err = proto.Unmarshal(ctx.Message.Payload(), &startProcessMessage)

		if err != nil {
			goto Response
		}

		process, err = s.LocalNode.StartProcessByExecutorName(startProcessMessage.Name, ProcessOptions{}, startProcessMessage.ExecutorName, startProcessMessage.Args)
		if err != nil {
			goto Response
		}
		fmt.Printf("Started process %s (PID: %s)", process.Name, process.PID)

	Response:
		var errorString string
		if err != nil {
			errorString = err.Error()
		}
		responseBytes, err := proto.Marshal(&systemProto.StartProcessResponseMessage{
			Success:      err == nil,
			ProcessID:    process.PID,
			ErrorMessage: errorString,
		})
		if err = ctx.Write(responseBytes); err != nil {
			println("Error sending response: " + err.Error())
		}
		_ = ctx.Conn.Close()
	})

	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		return err
	}

	go func() {
		router.Run(listener)
	}()
	return err
}
