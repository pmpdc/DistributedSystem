package System

type NodeOptions struct {
	GuestNode  bool
	PublicAddr string
	LocalPort  int
}
