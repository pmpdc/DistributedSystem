package System

import (
	"fmt"
	SystemProto "gitlab.com/pmpdc/DistributedSystem/proto"
)

func (s *System) Merge(remoteSystem *SystemProto.System) error {
	println("Merging nodes")
	println(fmt.Sprintf("\tRemote count: %d", len(remoteSystem.Nodes)))
	println(fmt.Sprintf("\tLocal count: %d", len(s.System.Nodes)))
	var err error
	var localNode *SystemProto.Node
	if s.LocalNode != nil {
		localNode = s.Nodes[s.LocalNode.ID]
	}
	s.Nodes = remoteSystem.Nodes

	if localNode != nil {
		s.Nodes[localNode.ID] = localNode
	}
	return err
}

func (s *System) Sync() {
	for _, node := range s.Nodes {
		if s.LocalNode != nil && node.ID == s.LocalNode.ID {
			continue
		}
		if s.SyncWith(node.PublicAddress, "") != nil {
			break
		}
	}
}

func (s *System) SyncWith(addr string, nodeSecretKey string) error {
	client, err := NewClient(addr, nodeSecretKey)
	if err != nil {
		println(err.Error())
		return err
	}

	remoteSystem, err := client.SyncSystem(s)
	if err != nil {
		println(err.Error())
		return err
	}
	return s.Merge(remoteSystem)
}

func (s *System) AddNode(addr, nodeSecretKey string) {
	s.wgNodes.Lock()
	defer s.wgNodes.Unlock()
	if _, exists := s.Nodes[addr]; exists {
		return
	}
	s.Nodes[addr] = &SystemProto.Node{
		ID:            "",
		Status:        NodeStatusNotSynced,
		PublicAddress: addr,
		Executors:     map[string]*SystemProto.ExecutorOptions{},
		Metadata:      map[string]string{},
	}
	_ = s.SyncWith(addr, nodeSecretKey)
}
