package System

import (
	"context"
	DSProto "gitlab.com/pmpdc/DistributedSystem/proto"
	"sync/atomic"
)

const (
	STATUS_SHUTTING_DOWN = "shutting_down"
)

type Node struct {
	DSProto.Node
	Options      NodeOptions
	executors    map[string]ExecutorInterface
	processes    map[string]*Process
	context      context.Context
	status       string
	shutdownChan chan bool
	processID    uint64
}

func (n *Node) GetExecutor(name string) ExecutorInterface {
	ex, _ := n.executors[name]
	return ex
}

func (n *Node) Update() {
	atomic.AddUint32(&n.Node.HeartBeat, 1)
}

func (n *Node) RegisterExecutor(name string, executor ExecutorInterface) {
	n.executors[name] = executor
	n.Executors[name] = &DSProto.ExecutorOptions{}
}

// RemoveExecutor wont stop runing process
func (n *Node) RemoveExecutor(name string) {
	if _, ok := n.executors[name]; ok {
		delete(n.executors, name)
	}
	if _, ok := n.Executors[name]; ok {
		delete(n.executors, name)
	}
}

func (n *Node) Shutdown() {
	n.status = STATUS_SHUTTING_DOWN
	n.context.Done()
	// TODO
}

func (n *Node) WaitUntilShutdown() {
	<-n.context.Done()
}
