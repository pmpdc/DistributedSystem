package System

import (
	"errors"
	"github.com/golang/protobuf/proto"
	systemProto "gitlab.com/pmpdc/DistributedSystem/proto"
	"gitlab.com/pmpdc/Network"
	"net"
	"time"
)

type client struct {
	addr   string
	client *Network.Client
}

func NewClient(addr string, secretKey string) (*client, error) {
	_client, err := Network.NewClient(func() (net.Conn, error) {
		return net.DialTimeout("tcp", addr, time.Second*3)
	})
	c := &client{
		addr:   addr,
		client: _client,
	}
	return c, err
}

func (c *client) SyncSystem(local *System) (*systemProto.System, error) {
	message := systemProto.SyncSystemMessage{
		System: &local.System,
	}
	var remoteSystem systemProto.SyncSystemMessage
	err := sendAndReceiveAsProtoBuff(c.client, SystemSyncPath, &message, &remoteSystem, time.Second*5)

	if err != nil && remoteSystem.Success == false {
		err = errors.New(remoteSystem.Error)
	}

	if err != nil {
		return nil, err
	}
	return remoteSystem.System, err
}

func (c client) StartProcess(processName string, executorName string, args []string) (*Process, error) {
	message := systemProto.StartProcessMessage{
		Name:         processName,
		Options:      &systemProto.ProcessOptions{ENV: map[string]string{}},
		ExecutorName: executorName,
		Args:         args,
	}

	var response systemProto.StartProcessResponseMessage
	err := sendAndReceiveAsProtoBuff(c.client, ProcessStartPath, &message, &response, time.Second*5)
	if err != nil && response.Success == false {
		err = errors.New(response.ErrorMessage)
	} else {
		process := &Process{
			PID:         response.ProcessID,
			Name:        processName,
			Options:     ProcessOptions{},
			parent:      nil,
			StartedIn:   time.Time{},
			FinishedIn:  time.Time{},
			Executioner: nil,
			Status:      "",
			KillChannel: nil,
			StopChannel: nil,
			finishChan:  nil,
		}
		return process, nil
	}
	return nil, err
}

// protobuff helpers
func sendAndReceiveAsProtoBuff(client *Network.Client, endpoint string, sendPayload proto.Message, receivePayload proto.Message, timeout time.Duration) error {
	sendBytes, err := proto.Marshal(sendPayload)
	var receiveBytes []byte
	if err := client.Call(endpoint, sendBytes, &receiveBytes, timeout); err != nil {
		return err
	}
	err = proto.Unmarshal(receiveBytes, receivePayload)
	if err != nil {
		return err
	}
	return nil
}
