package System

import (
	"context"
	"fmt"
	"github.com/anacrolix/sync"
	"gitlab.com/pmpdc/DistributedSystem/Utils"
	SystemProto "gitlab.com/pmpdc/DistributedSystem/proto"
	"gitlab.com/pmpdc/Network"
	"net"
	"strings"
)

const (
	NodeStatusNotSynced   = "not_synced"
	NodeStatusUnreachable = "unreachable"
	NodeStatusWaiting     = "waiting"
	NodeStatusActive      = "active"
	NodeStatusDead        = "dead"
)

type System struct {
	SystemProto.System
	LocalNode *Node
	router    *Network.Router
	wgNodes   sync.Mutex
}

func NewSystemNode(name string, options NodeOptions) (*System, error) {
	s := &System{
		System: SystemProto.System{
			ID:    "",
			Nodes: map[string]*SystemProto.Node{},
		},
		LocalNode: nil,
		wgNodes:   sync.Mutex{},
	}

	publicAddress := Utils.GetOutboundIP().String() + ":0"
	splited := strings.Split(name, "@")
	if len(splited) == 2 {
		publicAddress = splited[len(splited)-1]
		name = strings.Join(splited[:len(splited)-1], "@")
	}
	// TODO: prevent public addr from being localhost or 127.0.0.1
	if err := s.startLocalServer(options.LocalPort); err != nil {
		return nil, err
	}

	// find port in publicAddr
	if strings.HasSuffix(publicAddress, ":0") {
		publicAddress = strings.Replace(publicAddress, ":0", fmt.Sprintf(":%d", s.router.Listener.Addr().(*net.TCPAddr).Port), 1)
	}
	s.LocalNode = &Node{
		Node: SystemProto.Node{
			ID:            name,
			Status:        "",
			PublicAddress: "",
			Executors:     map[string]*SystemProto.ExecutorOptions{},
			Metadata:      map[string]string{},
		},
		Options:      options,
		executors:    map[string]ExecutorInterface{},
		processes:    map[string]*Process{},
		context:      context.Background(),
		status:       "",
		shutdownChan: nil,
		processID:    uint64(0),
	}
	s.LocalNode.PublicAddress = publicAddress

	s.Nodes[s.LocalNode.ID] = &s.LocalNode.Node
	s.LocalNode.Update()
	return s, nil
}
