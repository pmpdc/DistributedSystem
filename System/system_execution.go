package System

import (
	"errors"
)

// StartProcess This will pick a Node based on availability
func (s *System) StartProcess(processName string, options ProcessOptions, executorName string, args []string) (*Process, error) {
	for _, node := range s.Nodes {
		if _, exists := node.Executors[executorName]; exists {
			// if we are on the local node there's no need to use the network
			if node.ID == s.LocalNode.ID {
				return s.LocalNode.StartProcessByExecutorName(processName, options, executorName, args)
			}
			client, err := NewClient(node.PublicAddress, "")
			if err != nil {
				return nil, err
			}
			return client.StartProcess(processName, executorName, args)
		}
	}
	return nil, errors.New("Executor not found")
}
