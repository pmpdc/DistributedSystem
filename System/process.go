package System

import (
	"fmt"
	SystemProto "gitlab.com/pmpdc/DistributedSystem/proto"
	"log"
	"os"
	"strconv"
	"sync/atomic"
	"time"
)

type ProcessStatusType = string

const (
	ProcessStatusStarting = "starting"
	ProcessStatusRunning  = "running"
	ProcessStatusStopping = "stopping"
	ProcessStatusStopped  = "stopped"
)

type ProcessInterface interface {
	GetPID() string
	GetName() string
}

type ProcessOptions struct {
	SystemProto.ProcessOptions
}

type Process struct {
	PID         string
	Name        string
	Options     ProcessOptions
	parent      *Process
	children    []*Process
	StartedIn   time.Time
	FinishedIn  time.Time
	Executioner ExecutorInterface
	Status      ProcessStatusType
	KillChannel chan string
	StopChannel chan string
	finishChan  chan string
	Logger      *log.Logger
}

func (p *Process) Kill(reason string) error {
	for _, child := range p.children {
		if err := child.Kill("Parent process is being killed with reason: " + reason); err != nil {
			return err
		}
	}
	return p.Executioner.DeInit(reason)
}

func (p *Process) Wait() {
	<-p.finishChan
}

var processPID uint64 = 0

func NewProcess(name string, options ProcessOptions, executor ExecutorInterface, args []string, parent *Process) (*Process, error) {
	pid := atomic.AddUint64(&processPID, 1)
	p := &Process{
		PID:         strconv.FormatUint(pid, 10),
		Name:        name,
		Options:     options,
		parent:      parent,
		children:    []*Process{},
		Executioner: executor,
		Status:      ProcessStatusStarting,
		finishChan:  make(chan string),
		Logger:      log.New(os.Stdout, "", log.LstdFlags),
	}

	if parent != nil {
		parent.children = append(parent.children, p)
	}

	var cleanProcess = func() {
		close(p.finishChan)
	}
	go func() {
		defer func() {
			if err := recover(); err != nil { //catch
				p.Logger.Print(fmt.Sprintf("Process exception: %v", err))
				cleanProcess()
			}
		}()
		if err := executor.Init(p, args); err != nil {
			p.Logger.Print(fmt.Sprintf("Process exied with error: %v", err))
		} else {
			p.Logger.Print("Process exited successfully")
		}
		cleanProcess()
	}()

	return p, nil
}
