package DistributedSystem

import (
	"gitlab.com/pmpdc/DistributedSystem/System"
)

func NewNode(name string, options System.NodeOptions) (*System.System, error) {
	return System.NewSystemNode(name, options)
}
