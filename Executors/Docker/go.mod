module gitlab.com/pmpdc/DistributedSystem/Executors/Docker

go 1.17


require (
	github.com/containerd/containerd v1.5.1 // indirect
	github.com/docker/docker v20.10.6+incompatible // indirect
	github.com/docker/go-connections v0.4.0 // indirect

)
