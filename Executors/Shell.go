package Executors

import (
	"errors"
	"gitlab.com/pmpdc/DistributedSystem/System"
	"os/exec"
	"sync"
)

type SellExecutor struct {
	wg  sync.WaitGroup
	cmd *exec.Cmd
}

func (se *SellExecutor) Init(process *System.Process, args []string) (err error) {
	if len(args) == 0 {
		return errors.New("shell needs at least one parameter")
	}
	se.wg.Add(1)
	go func() {
		cmd := exec.Command(args[0], args[1:]...)
		err = cmd.Start()
		if err == nil {
			err = cmd.Wait()
		}
		se.wg.Done()
	}()
	se.wg.Wait()
	return err
}

func (se *SellExecutor) DeInit(reason string) error {
	if se.cmd != nil {
		return se.cmd.Process.Kill()
	}
	return nil
}
