package SecureEnclave

import (
	"errors"
	"gitlab.com/pmpdc/DistributedSystem/System"
)

type SecureEnclave struct {
	data map[string]string
}

func (se *SecureEnclave) Set(key string, value string) {
	se.data[key] = value
}

func (se *SecureEnclave) Get(key string) (string, error) {
	value, exists := se.data[key]
	if exists == false {
		return "", errors.New("key not found")
	}
	return value, nil
}

func (se *SecureEnclave) Init(process *System.Process, args []string) (err error) {
	if se.data == nil {
		se.data = map[string]string{}
	}
	return nil
}

func (se *SecureEnclave) DeInit(reason string) error {
	return nil
}
